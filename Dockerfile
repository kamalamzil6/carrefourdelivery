FROM adoptopenjdk/openjdk21:alpine-slim

COPY target/carrefourdelivery.jar /app/carrefourdelivery.jar

WORKDIR /app

EXPOSE 8090

CMD ["java", "-jar", "carrefourdelivery.jar"]