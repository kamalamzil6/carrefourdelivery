package com.carrefour.delivery.controller;

import com.carrefour.delivery.business.model.DeliveryPeriodDto;
import com.carrefour.delivery.service.DeliveryPeriodService;
import com.carrefour.delivery.service.impl.DeliveryPeriodServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/deliveryperiod")
@RequiredArgsConstructor
public class DeliveryPeriodController {

    private final DeliveryPeriodService deliveryPeriodService;

    /**
     * Récupère les périodes de livraison disponibles par mode de livraison
     *
     * @param deliveryMode L'identifiant du mode de livraison
     * @return  les périodes de livraison disponibles
     *      
     */
    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/list/{deliveryMode}", produces = MediaTypes.HAL_JSON_VALUE)
    public Mono<ResponseEntity<CollectionModel<EntityModel<DeliveryPeriodDto>>>> getAvailableDeliveryPeriodsByDeliveryMode(@PathVariable String deliveryMode) {
        return Mono.fromSupplier(() -> {
            List<DeliveryPeriodDto> deliveryPeriods = deliveryPeriodService.findAvailableDeliveryPeriodsByDeliveryMode(deliveryMode);
            List<EntityModel<DeliveryPeriodDto>> deliveryPeriodEntities = deliveryPeriods.stream()
                    .map(deliveryPeriodDto -> {
                        Link selfLink = Link.of("/deliveryperiod/" + LocalDateTime.of(deliveryPeriodDto.getStartDate(), deliveryPeriodDto.getStartTime()) + "/" + LocalDateTime.of(deliveryPeriodDto.getEndDate(), deliveryPeriodDto.getEndTime())).withSelfRel();
                        return EntityModel.of(deliveryPeriodDto, selfLink);
                    })
                    .toList();
            CollectionModel<EntityModel<DeliveryPeriodDto>> collectionModel = CollectionModel.of(deliveryPeriodEntities);
            return ResponseEntity.ok(collectionModel);
        });
    }

    /**
     * Sélectionne une période de livraison avec l'heure de début et l'heure de fin spécifiées
     *
     * @param startTime L'heure de début de la période de livraison
     * @param endTime   L'heure de fin de la période de livraison
     * @return  une chaîne de caractères représent la réponse de la sélection de la période de livraison
     */
   
    @GetMapping("/{startTime}/{endTime}")
    public Mono<ResponseEntity<String>> selectDeliveryPeriod(@PathVariable LocalDateTime startTime, @PathVariable LocalDateTime endTime) {
        return Mono.fromSupplier(() -> {
            String response = deliveryPeriodService.selectDeliveryPeriod(startTime, endTime);
            return ResponseEntity.ok(response);
        });
    }
}
