package com.carrefour.delivery.controller;

import com.carrefour.delivery.business.model.DeliveryModeDto;
import com.carrefour.delivery.service.DeliveryModeService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/deliverymode")
@RequiredArgsConstructor
public class DeliveryModeController {

    private final DeliveryModeService deliveryModeService;

    /**
     * Récupère tous les modes de livraison disponibles
     *
     * @return  les modes de livraison disponibles
     */
    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/list", produces = MediaTypes.HAL_JSON_VALUE)
    public Mono<ResponseEntity<CollectionModel<EntityModel<DeliveryModeDto>>>> getAllDeliveryModes() {
        Flux<DeliveryModeDto> deliveryModes = Flux.fromIterable(deliveryModeService.findAllDeliveryModes());

   
        Flux<EntityModel<DeliveryModeDto>> deliveryModeEntities = deliveryModes.flatMap(deliveryModeDto -> {
            String deliveryModeName = deliveryModeDto.getName();
            Link selfLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DeliveryModeController.class)
                    .selectDeliveryMode(deliveryModeName)).withSelfRel();
            return Mono.just(EntityModel.of(deliveryModeDto, selfLink));
        });


        Mono<CollectionModel<EntityModel<DeliveryModeDto>>> collectionModelMono = deliveryModeEntities.collectList().map(CollectionModel::of);

        return collectionModelMono.map(ResponseEntity::ok);
    }

    /**
     * Sélectionne un mode de livraison spécifique.
     *
     * @param deliveryMode Le mode de livraison à sélectionner.
     * @return une chaîne de caractères représent la réponse de la sélection du mode de livraison
     */
    @PreAuthorize("hasRole('USER')")
    @GetMapping("/{deliveryMode}")
    public Mono<ResponseEntity<String>> selectDeliveryMode(@PathVariable String deliveryMode) {
        Mono<String> response = Mono.just(deliveryModeService.selectDeliveryMode(deliveryMode));
        return response.map(ResponseEntity::ok);
    }
}
