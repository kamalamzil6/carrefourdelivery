package com.carrefour.delivery.repository;

import com.carrefour.delivery.business.entity.DeliveryPeriod;
import com.carrefour.delivery.types.DeliveryModeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeliveryPeriodRepository extends JpaRepository<DeliveryPeriod, Long> {
    
    List<DeliveryPeriod> findByDeliveryModeAndAvailable(DeliveryModeEnum deliveryMode, boolean available);
}
