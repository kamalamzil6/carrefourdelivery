package com.carrefour.delivery.repository;

import com.carrefour.delivery.business.entity.DeliveryMode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryModeRepository extends JpaRepository<DeliveryMode, Long> {
}
