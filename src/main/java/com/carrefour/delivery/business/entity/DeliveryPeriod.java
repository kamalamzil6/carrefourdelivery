package com.carrefour.delivery.business.entity;

import com.carrefour.delivery.types.DeliveryModeEnum;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table (name= "DELIVERY_PERIOD")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryPeriod implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private DeliveryModeEnum deliveryMode;
    @Column
    private boolean available; // Indique si le créneau horaire est disponible ou réservé
    @Column
    private LocalDateTime startTime;
    @Column
    private LocalDateTime endTime;

}
