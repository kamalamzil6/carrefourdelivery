package com.carrefour.delivery.business.entity;

import com.carrefour.delivery.types.DeliveryModeEnum;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Entity
@Table(name = "DELIVERY_MODE" )
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryMode implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private DeliveryModeEnum name;
}
