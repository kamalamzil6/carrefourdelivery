package com.carrefour.delivery.business.model;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class DeliveryPeriodDto extends RepresentationModel<DeliveryPeriodDto> {

    private LocalDate startDate;
    private LocalTime startTime;
    private LocalDate endDate;
    private LocalTime endTime;
    private String deliveryMode;
    private boolean available;
}
