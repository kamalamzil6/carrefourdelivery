package com.carrefour.delivery.business.model;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
public class DeliveryModeDto extends RepresentationModel<DeliveryModeDto> {

    String name;
}
