package com.carrefour.delivery;

import com.carrefour.delivery.business.entity.DeliveryMode;
import com.carrefour.delivery.business.entity.DeliveryPeriod;
import com.carrefour.delivery.repository.DeliveryModeRepository;
import com.carrefour.delivery.repository.DeliveryPeriodRepository;
import com.carrefour.delivery.types.DeliveryModeEnum;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import java.time.LocalDateTime;
import java.util.Arrays;

@SpringBootApplication
@EnableCaching
@RequiredArgsConstructor
public class CarrefourDeliveryApplication implements CommandLineRunner {

    private final Logger log =LoggerFactory.getLogger(getClass());
    private final DeliveryModeRepository deliveryModeRepository;
    private final DeliveryPeriodRepository deliveryPeriodRepository;

    public static void main(String[] args) {

        SpringApplication.run(CarrefourDeliveryApplication.class, args);
    }

    @Override
    public void run(String... args) {

        log.info("Saving delivery modes");

        DeliveryMode deliveryMode1 = new DeliveryMode();
        deliveryMode1.setName(DeliveryModeEnum.DRIVE);

        DeliveryMode deliveryMode2 = new DeliveryMode();
        deliveryMode2.setName(DeliveryModeEnum.DELIVERY);

        DeliveryMode deliveryMode3 = new DeliveryMode();
        deliveryMode3.setName(DeliveryModeEnum.DELIVERY_TODAY);

        DeliveryMode deliveryMode4 = new DeliveryMode();
        deliveryMode4.setName(DeliveryModeEnum.DELIVERY_ASAP);

        deliveryModeRepository.saveAll(Arrays.asList(deliveryMode1, deliveryMode2, deliveryMode3, deliveryMode4));

        log.info("Saving delivery periods");

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime oneHourLater = now.plusHours(1);
        LocalDateTime twoHoursLater = now.plusHours(2);
        LocalDateTime threeHoursLater = now.plusHours(3);
        LocalDateTime fourHoursLater = now.plusHours(4);

        DeliveryPeriod deliveryPeriod1 = new DeliveryPeriod();
        deliveryPeriod1.setDeliveryMode(DeliveryModeEnum.DRIVE);
        deliveryPeriod1.setAvailable(true);
        deliveryPeriod1.setStartTime(now);
        deliveryPeriod1.setEndTime(oneHourLater);

        DeliveryPeriod deliveryPeriod2 = new DeliveryPeriod();
        deliveryPeriod2.setDeliveryMode(DeliveryModeEnum.DELIVERY);
        deliveryPeriod2.setAvailable(true);
        deliveryPeriod2.setStartTime(oneHourLater);
        deliveryPeriod2.setEndTime(twoHoursLater);

        DeliveryPeriod deliveryPeriod3 = new DeliveryPeriod();
        deliveryPeriod3.setDeliveryMode(DeliveryModeEnum.DELIVERY_TODAY);
        deliveryPeriod3.setAvailable(true);
        deliveryPeriod3.setStartTime(twoHoursLater);
        deliveryPeriod3.setEndTime(threeHoursLater);

        DeliveryPeriod deliveryPeriod4 = new DeliveryPeriod();
        deliveryPeriod4.setDeliveryMode(DeliveryModeEnum.DELIVERY_ASAP);
        deliveryPeriod4.setAvailable(true);
        deliveryPeriod4.setStartTime(threeHoursLater);
        deliveryPeriod4.setEndTime(fourHoursLater);

        deliveryPeriodRepository.saveAll(Arrays.asList(deliveryPeriod1, deliveryPeriod2, deliveryPeriod3, deliveryPeriod4));

        log.info("Done Saving");
    }
}
