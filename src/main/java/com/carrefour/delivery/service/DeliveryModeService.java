package com.carrefour.delivery.service;

import com.carrefour.delivery.business.model.DeliveryModeDto;

import java.util.List;

public interface DeliveryModeService {

    List<DeliveryModeDto> findAllDeliveryModes();

    String selectDeliveryMode(String deliveryMode);
}
