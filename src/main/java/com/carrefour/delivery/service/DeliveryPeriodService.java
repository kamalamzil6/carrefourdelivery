package com.carrefour.delivery.service;

import com.carrefour.delivery.business.model.DeliveryPeriodDto;

import java.time.LocalDateTime;
import java.util.List;

public interface DeliveryPeriodService {


    List<DeliveryPeriodDto> findAvailableDeliveryPeriodsByDeliveryMode(String deliveryMode);

    String selectDeliveryPeriod(LocalDateTime startTime, LocalDateTime endTime);
}