package com.carrefour.delivery.service.impl;

import com.carrefour.delivery.business.entity.DeliveryPeriod;
import com.carrefour.delivery.business.model.DeliveryPeriodDto;
import com.carrefour.delivery.repository.DeliveryPeriodRepository;
import com.carrefour.delivery.service.DeliveryPeriodService;
import com.carrefour.delivery.types.DeliveryModeEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DeliveryPeriodServiceImpl implements DeliveryPeriodService {

    private final DeliveryPeriodRepository deliveryPeriodRepository;

    @Override
    @Cacheable(value = "DeliveryPeriods")
    public List<DeliveryPeriodDto> findAvailableDeliveryPeriodsByDeliveryMode(String deliveryMode) {

        List<DeliveryPeriod> deliveryPeriods = deliveryPeriodRepository.findByDeliveryModeAndAvailable(DeliveryModeEnum.valueOf(deliveryMode), true);

        return deliveryPeriods.stream()
                .map(this::mapToDto)
                .toList();
    }

    @Override
    @Cacheable(value = "DeliveryPeriod")
    public String selectDeliveryPeriod(LocalDateTime startTime, LocalDateTime endTime) {

        DeliveryPeriod deliveryPeriod=new DeliveryPeriod();
        deliveryPeriod.setStartTime(startTime);
        deliveryPeriod.setEndTime(endTime);
        deliveryPeriod.setAvailable(false);

        deliveryPeriodRepository.save(deliveryPeriod);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy à HH:mm");

        String formattedStartTime = deliveryPeriod.getStartTime().format(formatter);
        String formattedEndTime = deliveryPeriod.getEndTime().format(formatter);

        return "Le créneau horaire que vous avez sélectionné est entre " + formattedStartTime + " et " + formattedEndTime;
    }


    private DeliveryPeriodDto mapToDto(DeliveryPeriod deliveryPeriod) {

        DeliveryPeriodDto dto = new DeliveryPeriodDto();
        dto.setStartTime(deliveryPeriod.getStartTime().toLocalTime());
        dto.setStartDate(deliveryPeriod.getStartTime().toLocalDate());
        dto.setEndTime(deliveryPeriod.getEndTime().toLocalTime());
        dto.setEndDate(deliveryPeriod.getEndTime().toLocalDate());
        dto.setDeliveryMode(deliveryPeriod.getDeliveryMode().getValue());

        return dto;
    }
}
