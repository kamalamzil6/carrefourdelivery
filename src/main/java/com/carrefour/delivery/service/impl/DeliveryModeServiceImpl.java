package com.carrefour.delivery.service.impl;

import com.carrefour.delivery.business.entity.DeliveryMode;
import com.carrefour.delivery.business.model.DeliveryModeDto;
import com.carrefour.delivery.repository.DeliveryModeRepository;
import com.carrefour.delivery.service.DeliveryModeService;
import com.carrefour.delivery.types.DeliveryModeEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeliveryModeServiceImpl implements DeliveryModeService {

    private final DeliveryModeRepository deliveryModeRepository;

    @Override
    @Cacheable(value = "DeliveryModes")
    public List<DeliveryModeDto> findAllDeliveryModes() {

        List<DeliveryMode> deliveryModes = deliveryModeRepository.findAll();

        return deliveryModes.stream()
                .map(this::mapToDto)
                .toList();
    }

    @Override
    @Cacheable(value = "DeliveryMode")
    public String selectDeliveryMode(String deliveryMode) {

        return "Le mode de livraison que vous avez sélectionné est :" + DeliveryModeEnum.valueOf(deliveryMode);
    }


    private DeliveryModeDto mapToDto(DeliveryMode deliveryMode) {
        DeliveryModeDto dto = new DeliveryModeDto();
        dto.setName(deliveryMode.getName().getValue());
        return dto;
    }
}
