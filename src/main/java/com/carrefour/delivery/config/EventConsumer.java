package com.carrefour.delivery.config;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class EventConsumer {

    @KafkaListener(topics = "events", groupId = "group-id")
    public void consumeEvent(String message) {
        System.out.println("Received event: " + message);
    }
}