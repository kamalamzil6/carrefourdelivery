package com.carrefour.delivery.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class EventProducer {

    private static final String TOPIC = "events";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void produceEvent(String message) {
        kafkaTemplate.send(TOPIC, message);
    }
}