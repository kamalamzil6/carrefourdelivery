package com.carrefour.delivery.types;

public enum DeliveryModeEnum {
    DRIVE("DRIVE"),
    DELIVERY("DELIVERY"),
    DELIVERY_TODAY("DELIVERY_TODAY"),
    DELIVERY_ASAP("DELIVERY_ASAP");


    private String value;

    DeliveryModeEnum(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
