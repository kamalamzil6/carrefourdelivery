<h1>Carrefour Java kata -- Delivery</h1> 

<h3>Candidat: Kamal AMZIL</h3>

<p>Ce document fournit des instructions sur la manière de lancer le projet et <b style="color:#FF0000";>des réponses complémentaires aux questions posées.</b></p>

<h2> Comment lancer le projet </h2>

    - Utiliser JDK 21
    - Cloner le projet sur votre machine locale
    - Importer les dépendances du fichier pom.xml
    - Lancer le cycle Maven via la commande clean-install
    - Démarrer le projet

Le projet utilise une base de données H2 pour migrer les tables et initialiser les données.
L'initialisation des données est effectuée dans la classe main <b>CarrefourDeliveryApplication.java</b>.

L'application est sécurisée avec les identifiants suivants : <b> login : "user" / mot de passe : "user" </b>.

Note: J'ai créé toutes les Api Rest en @GetMapping afin de faciliter le test de l'application depuis le navigateur.

<h3>Des urls de l'Appli :</h3>

    1- Liste des modes de livraison : 
            http://localhost:8090/deliverymode/list

    2- Disponibilité des créneaux :
        Drive : http://localhost:8090/deliveryperiod/list/DRIVE
        DELIVERY : http://localhost:8090/deliveryperiod/list/DELIVERY
        DELIVERY_TODAY : http://localhost:8090/deliveryperiod/list/DELIVERY_TODAY
        DELIVERY_ASAP : http://localhost:8090/deliveryperiod/list/DELIVERY_ASAP

Chaque objet dans la liste des modes de livraison et des créneaux contient des liens conformes aux principes HATEOAS pour faciliter la navigation.

<h2>réponses complémentaires  aux questions</h2>

<h3>User Story 1 : Choix du mode de livraison</h3>

L'implémentation de cette fonctionnalité se trouve dans :

    Contrôleur REST : DeliveryModeController.java
    Service : DeliveryModeService.java
    Repository : DeliveryModeRepository.java
    Entité : DeliveryMode.java

<h3>User Story 2 : Choix du jour et du créneau horaire</h3>

L'implémentation de cette fonctionnalité se trouve dans :

    Contrôleur REST : DeliveryPeriodController.java
    Service : DeliveryPeriodService.java
    Repository : DeliveryPeriodRepository.java
    Entité : DeliveryPeriod.java

<h3>Les API REST</h3>

    1- Pour interagir avec les services du MVP, les endpoints suivants sont disponibles :
        - Liste des modes de livraison : http://localhost:8090/deliverymode/list
        - Liste des créneaux de livraison pour DRIVE : http://localhost:8090/deliveryperiod/list/DRIVE
        - Liste des créneaux de livraison pour DELIVERY : http://localhost:8090/deliveryperiod/list/DELIVERY
        - Liste des créneaux de livraison pour DELIVERY_TODAY : http://localhost:8090/deliveryperiod/list/DELIVERY_TODAY
        - Liste des créneaux de livraison pour DELIVERY_ASAP : http://localhost:8090/deliveryperiod/list/DELIVERY_ASAP
    
    2- Les principes HATEOAS sont implémentés dans les contrôleurs DeliveryModeController et DeliveryPeriodController.

    3- L'API est sécurisée via la configuration dans la classe WebSecurityConfig avec les identifiants : login : "user" / mot de passe : "user".

    4- Une solution non-bloquante est mise en place en utilisant Mono et Flux dans les contrôleurs DeliveryModeController et DeliveryPeriodController.

    5- La documentation de l'API REST est réalisée via Javadoc pour toutes les méthodes des contrôleurs.

<h3>Persistence</h3>

    Les données sont persistées en utilisant JPA et une base de données H2 est utilisée comme base de données mémoire.
        Repository : DeliveryModeRepository, DeliveryPeriodRepository
        Entités : DeliveryMode, DeliveryPeriod

    Le cache est activé via @EnableCaching dans la classe main, et l'annotation @Cacheable est utilisée dans les méthodes de services.

<h3>Streaming de données</h3>

    Utilisation de Kafka Streams pour la gestion des flux en temps réel 
     La configuration de Kafka Streams se trouve dans la classe KafkaStreamsConfig, 
     permettant de mettre en place un service de traitement de données en temps réel.

    Utilisation d'Apache Kafka pour la consommation et la production d'événements 
    J'ai créé deux composants, EventProducer et EventConsumer.

     * EventProducer est responsable de la production d'événements et envoie les messages aux topics Kafka appropriés.
     *  EventConsumer est chargé de la consommation des événements à partir des topics Kafka.


<h3>CI/CD</h3>

    Un système CI/CD est mis en place via le fichier .gitlab-ci.yml contenant les scripts pour les 3 stages : builder, test, install avec différents jobs de tests.

<h3>Tests End to End (E2E)</h3>

    En raison de contraintes de temps, nous pouvons créer un module dédié pour exécuter 
    des tests End to End en utilisant les tests <b> Karate </b>.

Voici un exemple de scénario de test :


    Scenario Outline: nombre des modes de livraison disponibles depuis database
        Given url http://localhost:8090
        Given path '/deliverymode/list'
        And header Authorization = "Authorization: Basic user:user
        When method POST
        Then status <codeStatus>
        And assert response != null
        And assert response._embedded.deliveryModeDtoList.length  == 4
        Examples:
        |codeStatus|
        | 201      |

<h3>Packaging - fichier "dockerfile.txt"</h3>
Création d'un container Docker

    Un exemple de fichier Dockerfile est fourni dans le fichier dockerfile.
    Pour construire l'image Docker :

    > docker build -t carrefourdelivery

    Pour lancer le conteneur :

    > docker run -d -p 8090:8090 carrefourdelivery

    //!\\ On peut inclure ces deux commandes dans le script .gitlab-ci.yml pour lancer le conteneur à chaque fois qu'on lance les pipelines.

<h3>Déploiement dans un pod Kubernetes - fichier deployment.txt</h3>

    Un exemple de fichier deployment.yaml est fourni pour le déploiement dans un pod Kubernetes.
    Pour déployer l'application :

   > kubectl apply -f deployment.yaml

    //!\\ On peut inclure cette commande dans le script .gitlab-ci.yml pour lancer le déploiment à chaque fois qu'on lance les pipelines.

<h3>Création d'une image native</h3>

    Le plugin nécessaire pour créer une image native on l'ajoute dans le fichier pom.xml.
        <plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>build-image</goal>
						</goals>
						<configuration>
							<imageName>carrefourdelivery</imageName>
						</configuration>
					</execution>
				</executions>
			</plugin>

    Pour créer l'image native :

    > mvn spring-boot:build-image -Dspring-boot.build-image.builder=native-image

